# ###            HOSTNAME          ###
_HOSTNAME="$(get_cmdline_param hostname)"

if [ ! -f $PERSISTENCE_ROOT/etc/hosts ]; then
    cat > /etc/hosts <<EOF
127.0.0.1       localhost
127.0.1.1       $_HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF
fi

add_pertistant_storage /etc/hosts bind

if [ ! -f $PERSISTENCE_ROOT/etc/hosts ]; then
    mkdir -p $PERSISTENCE_ROOT/etc
    echo "$_HOSTNAME" > $PERSISTENCE_ROOT/etc/hosts
fi

add_pertistant_storage /etc/hostname bind

if [ "$_HOSTNAME" != "$HOSTNAME" ]; then
    # NOTE: ignore hostnamectl error due to this autistic tool try to remove
    # /etc/hostname and create a new one instead of simply write into it... o_O
    hostnamectl set-hostname "$_HOSTNAME" 2> /dev/null || true
    echo "$_HOSTNAME" > /etc/hostname
    hostname "$_HOSTNAME"
    if [ "$HOSTNAME" != "localhost" ]; then
        sed -i "s/$HOSTNAME/$_HOSTNAME/g" /etc/hosts
    fi
    export HOSTNAME="$_HOSTNAME"
fi
